﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Configuration;

namespace SeleniumFramework.Helpers
{
    public class BrowserFactory
    {
        public static IWebDriver _driver;
        private static string _baseURL = ConfigurationManager.AppSettings["url"];
        private static string _browser = ConfigurationManager.AppSettings["browser"];

        public static void Init()
        {
            switch (_browser.ToLower())
            {
                case "chrome":
                    _driver = new ChromeDriver();
                    break;
                case "ie":
                    _driver = new InternetExplorerDriver();
                    break;
                case "firefox":
                    _driver = new FirefoxDriver();
                    break;
            }

            _driver.Manage().Window.Maximize();
            GoTo(_baseURL);
        }

        public static void GoTo(string url)
        {
            _driver.Url = url;
        }
        public static void CloseBrowser()
        {
            if (_driver != null)
            {
                _driver.Quit();
            }
        }
    }
}
