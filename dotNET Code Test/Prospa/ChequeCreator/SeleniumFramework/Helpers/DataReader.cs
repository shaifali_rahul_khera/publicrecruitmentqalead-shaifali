﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SeleniumFramework.TestData.Templates;
using System.Collections;
using System.Configuration;

namespace SeleniumFramework.Helpers
{
    class DataReader
    {
        private static string _dataFile = ConfigurationManager.AppSettings["dataFileName"];
        public static IEnumerable FetchData
        {
            get
            {
                string currentDirectoryPath = AppDomain.CurrentDomain.BaseDirectory;
                currentDirectoryPath = currentDirectoryPath + "\\..\\..\\TestData\\DataFiles\\";
                var filePath = currentDirectoryPath + _dataFile;
                var reader = new CsvHelper.CsvReader(File.OpenText(filePath));
                while (reader.Read())
                {
                    var data = reader.GetRecord<AddChequesDataClass>();
                    yield return new[] {data};
                }
            }
        }
    }
}
