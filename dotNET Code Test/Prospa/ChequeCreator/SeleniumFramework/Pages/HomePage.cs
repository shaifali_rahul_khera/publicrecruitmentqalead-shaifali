﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;
using SeleniumFramework.Helpers;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using AventStack.ExtentReports;

namespace SeleniumFramework.Pages
{
    public class HomePage : BrowserFactory
    {
        public IWebDriver driver;
        private IWait<IWebDriver> wait;

        [FindsBy(How = How.XPath, Using = "//button[text()='Add a cheque']")]
        public IWebElement BtnAddACheque { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".cheque-pad-pay input")]
        public IWebElement InputPayTo { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".cheque-pad-date input")]
        public IWebElement InputChequeDate { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".cheque-pad-amount input")]
        public IWebElement InputChequeAmount { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".cheque-pad-sumof p")]
        public IWebElement PAmountInWords { get; set; }

        [FindsBy(How = How.XPath, Using = "//div/button[text()='Add a cheque']")]
        public IWebElement BtnAddAChequeAfterChequeAdded { get; set; }

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30.00));
        }

        public void ClickAddACheque()
        {
            BtnAddACheque.Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(InputPayTo));
        }

        public void EnterPayeeName(string pay)
        {
            InputPayTo.SendKeys(pay);
        }

        public void EnterDate(string date)
        {
            InputChequeDate.Click();
            InputChequeDate.SendKeys(Keys.ArrowLeft);
            InputChequeDate.SendKeys(Keys.ArrowLeft);
            InputChequeDate.SendKeys(date);
        }

        public void EnterAmount(string amount)
        {
            InputChequeAmount.SendKeys(amount);
        }

        public ExtentTest ValidateSumToText(string expectedText, ExtentTest test)
        {
            InputPayTo.Click();
            if (PAmountInWords.Text == expectedText)
            {
                test.Pass("The Sum To label matches to expected value i.e.:" + expectedText);
            }
            else
            {
                test.Fail("The Sum to label doesn't match to expected value i.e.:" + expectedText);
            }
            return test;
        }

        public void IncrementAmount()
        {
            InputChequeAmount.Click();
            InputChequeAmount.SendKeys(Keys.ArrowUp);
        }

        public void DecrementAmount()
        {
            InputChequeAmount.Click();
            InputChequeAmount.SendKeys(Keys.ArrowDown);
        }

        public ExtentTest ValidateAmount(string expectedAmount, ExtentTest test)
        {
            InputPayTo.Click();
            if (InputChequeAmount.GetAttribute("value") == expectedAmount)
            {
                test.Pass("The amount displayed to expected value i.e.:" + expectedAmount);
            }
            else
            {
                test.Fail("The amount displayed doesn't match to expected value i.e.:" + expectedAmount);
            }
            return test;
        }

        public ExtentTest ValidateDate(string expectedDate, ExtentTest test)
        {
            InputPayTo.Click();
            if (InputChequeDate.GetAttribute("value") == expectedDate)
            {
                test.Pass("The date displayed to expected value i.e.:" + expectedDate);
            }
            else
            {
                test.Fail("The date displayed doesn't match to expected value i.e.:" + expectedDate);
            }
            return test;
        }

        public void ClickAddAChequeAfterChequeAdded()
        {
            BtnAddAChequeAfterChequeAdded.Click();
        }
    }
}
