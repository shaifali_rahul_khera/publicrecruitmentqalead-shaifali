﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumFramework.Helpers;

namespace SeleniumFramework.TestData.Templates
{
    public class AddChequesDataClass
    {
        public string Pay { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }
        public string ExpectedSumTo { get; set; }
    }
}
