﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;
using OpenQA.Selenium.Support.Extensions;
using SeleniumFramework.Helpers;
using SeleniumFramework.TestData.Templates;
using SeleniumFramework.Pages;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework.Interfaces;
using System.Configuration;
using System.Globalization;

namespace SeleniumFramework.Tests
{
    [TestFixture]
    public class AddChequeTests:BrowserFactory
    {
        public HomePage Home;
        public ExtentReports Extent = new ExtentReports();
        public static ExtentTest Test;
        public string Status;
        public string ErrorMessage;
        private static string _reportFileName = ConfigurationManager.AppSettings["reportFileName"];
        public string FilePath;
        public static ExtentHtmlReporter HtmlReporter;

        [SetUp]
        public void Initialize()
        {
            BrowserFactory.Init();
            Home = new HomePage(_driver);
            FilePath = AppDomain.CurrentDomain.BaseDirectory + "..\\..\\Reports\\" + _reportFileName;
            HtmlReporter = new ExtentHtmlReporter(FilePath);
            Extent.AttachReporter(HtmlReporter);
        }

        [Test, TestCaseSource(typeof(DataReader), "FetchData")]
        public void AddACheque(AddChequesDataClass data)
        {
            Test = Extent.CreateTest("Testing adding a cheque");
            Home.ClickAddACheque();
            Home.EnterPayeeName(data.Pay);
            Home.EnterDate(data.Date);
            Home.EnterAmount(data.Amount);
            Test = Home.ValidateSumToText(data.ExpectedSumTo, Test);
            Status = Test.Status.ToString();
            ErrorMessage = TestContext.CurrentContext.Result.Message;
            WriteTestStatus(Status, ErrorMessage, Test);
        }

        [Test]
        public void IncrementAmount()
        {
            Test = Extent.CreateTest("Testing incrementing the value in amount field");
            Home.ClickAddACheque();
            Home.EnterPayeeName("Automation");
            Home.EnterDate("12/21/2017");
            Home.EnterAmount("45");
            Home.IncrementAmount();
            Test = Home.ValidateAmount("46", Test);
            Test = Home.ValidateSumToText("Forty-six dollars and zero cents", Test);
            Status = Test.Status.ToString();
            ErrorMessage = TestContext.CurrentContext.Result.Message;
            WriteTestStatus(Status, ErrorMessage, Test);
        }

        [Test]
        public void DecrementAmount()
        {
            Test = Extent.CreateTest("Testing decrementing the value in amount field");
            Home.ClickAddACheque();
            Home.EnterPayeeName("Automation");
            Home.EnterDate("12/21/2017");
            Home.EnterAmount("45");
            Home.DecrementAmount();
            Test = Home.ValidateAmount("44", Test);
            Test = Home.ValidateSumToText("Forty-four dollars and zero cents", Test);
            Status = Test.Status.ToString();
            ErrorMessage = TestContext.CurrentContext.Result.Message;
            WriteTestStatus(Status, ErrorMessage, Test);
        }

        [Test]
        public void DateBoundaryValueCheck()
        {
            Test = Extent.CreateTest("Testing boundary value + 1 values to mm, dd and yyyy in Date field");
            Home.ClickAddACheque();
            Home.EnterPayeeName("Automation");
            Home.EnterAmount("45");
            Home.EnterDate("13/21/2017");
            Test = Home.ValidateDate("2017-12-21", Test);
            Home.EnterDate("12/32/2017");
            Test = Home.ValidateDate("2017-12-31", Test);
            Home.EnterDate("02/31/2017");
            Test = Home.ValidateDate("2017-02-31", Test);
            Home.EnterDate("00/00/0000");
            Test = Home.ValidateDate("0001/01/01", Test);
            Status = Test.Status.ToString();
            ErrorMessage = TestContext.CurrentContext.Result.Message;
            WriteTestStatus(Status, ErrorMessage, Test);
        }

        [Test]
        public void AddMultipleCheques()
        {
            Test = Extent.CreateTest("Testing adding multiple cheques");
            Home.ClickAddACheque();
            for (int index = 0; index <= 10; index++)
            {
                Home.ClickAddAChequeAfterChequeAdded();
            }
            Status = Test.Status.ToString();
            ErrorMessage = TestContext.CurrentContext.Result.Message;
            WriteTestStatus(Status, ErrorMessage, Test);
        }

        [TearDown]
        public void EndTest()
        {
            Extent.Flush();
            BrowserFactory.CloseBrowser();
        }

        public void WriteTestStatus(string status, string errorMessage, ExtentTest test)
        {
            if (status.ToString() == "Fail")
            {
                Test.Fail(status + errorMessage);
            }
            else if (status.ToString() == "Pass")
            {
                Test.Pass("Test Passed");
            }
        }

    }
}
