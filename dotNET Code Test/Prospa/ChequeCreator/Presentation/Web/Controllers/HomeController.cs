﻿using System.Web.Mvc;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorOrchestration;

namespace Prospa.ChequeCreator.Presentation.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICurrencyTranslatorOrchestration _currencyTranslatorOrchestration;

        public HomeController(ICurrencyTranslatorOrchestration currencyTranslatorOrchestration)
        {
            _currencyTranslatorOrchestration = currencyTranslatorOrchestration;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Create a cheque.";

            return View();
        }


    }
}